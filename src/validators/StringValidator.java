package validators;

import entities.StringEntity;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class StringValidator {

    private StringEntity entity;
    private Set<String> results;

    public StringValidator(StringEntity entity){
        this.entity = entity;
    }

    public Set<String> validate(){
        results = new HashSet<>();
        if(entity.getDifference() == 0) results.add(entity.getInput());
        else findResults();
        return results;
    }

    private void findResults(){
        List<Integer> positions;
        char searchCharacter;
        int difference = Math.abs(entity.getDifference());
        if (entity.getDifference() < 0) {
            positions = entity.getClosingBracketsPositions();
            searchCharacter = '}';
        }
        else {
            positions = entity.getOpeningBracketsPositions();
            searchCharacter = '{';
        }
        iterateOverOptions(positions, searchCharacter, difference);
    }

    private void iterateOverOptions(List<Integer> positions, char searchCharacter, int difference){
        for(int i = 0; i < positions.size(); i++){
            int position = positions.get(i);
            StringBuilder buffer = new StringBuilder(entity.getInput());
            buffer.deleteCharAt(position);
            findAndSaveOptions(buffer, difference, position, searchCharacter);
        }
    }

    private void findAndSaveOptions(StringBuilder buffer, int difference, int position, char searchCharacter){
        int pointer = position;
        int deletingCounter = difference - 1;
        String sourceString = buffer.toString();
        boolean flag = true;
        boolean pointerMoved = false;
        while (flag){
            if(pointer > buffer.length() - 1) pointer = 0;
            if(pointer == position){
                if(pointerMoved) flag = false;
            }
            if(position > buffer.length() - 1) flag = false;
            if(buffer.charAt(pointer) == searchCharacter) {
                buffer.deleteCharAt(pointer);
                deletingCounter--;
            }
            else {
                pointer++;
                pointerMoved = true;
            }
            if(deletingCounter == 0){
                if(!haveWasteBrackets(buffer.toString())) {
                    results.add(buffer.toString());
                    buffer = new StringBuilder(sourceString);
                    deletingCounter = difference - 1;
                    pointer++;
                    pointerMoved = true;
                }
            }
        }
    }

    private boolean haveWasteBrackets(String input){
        char[] chars = input.toCharArray();
        for(int i = 0; i < chars.length; i++){
            if(chars[i] == '{') break;
            if(chars[i] == '}') return true;
        }
        for(int i = chars.length - 1; i >= 0; i--){
            if(chars[i] == '}') break;
            if(chars[i] == '{') return true;
        }
        return false;
    }
}
