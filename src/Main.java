import entities.StringEntity;
import validators.StringValidator;

import java.util.Set;

public class Main {

    public static Set<String> validate(String input){
        StringEntity entity = new StringEntity(input);
        StringValidator stringValidator = new StringValidator(entity);
        return stringValidator.validate();
    }

    public static void main(String[] args) {
        String input = "{}x}x}";
        Set<String> results = validate(input);
        System.out.println("Source string: " + input);
        System.out.println("Results: ");
        for(String str : results){
            System.out.println(str);
        }
    }
}
