package entities;

import java.util.ArrayList;
import java.util.List;

public class StringEntity {

    private String input;
    private List<Integer> openingBracketsPositions;
    private List<Integer> closingBracketsPositions;

    public StringEntity(String input){
        this.input = deleteWasteBrackets(input);
        initializeBracketsPositions();
    }

    public int getDifference(){
        return openingBracketsPositions.size() - closingBracketsPositions.size();
    }

    public String getInput() {
        return input;
    }

    public List<Integer> getOpeningBracketsPositions() {
        return openingBracketsPositions;
    }

    public List<Integer> getClosingBracketsPositions() {
        return closingBracketsPositions;
    }

    private String deleteWasteBrackets(String input){
        StringBuilder stringBuilder = new StringBuilder(input);
        char[] chars = input.toCharArray();
        int positionCounter = 0;
        for(int i = 0; i < chars.length; i++){
            if(chars[i] == '{') break;
            if(chars[i] == '}') {
                stringBuilder.deleteCharAt(i - positionCounter);
                positionCounter++;
            }
        }
        chars = stringBuilder.toString().toCharArray();
        for(int i = chars.length - 1; i >= 0; i--){
            if(chars[i] == '}') break;
            if(chars[i] == '{') {
                stringBuilder.deleteCharAt(i);
            }
        }
        return stringBuilder.toString();
    }

    private void initializeBracketsPositions(){
        char[] characterSequence  = input.toCharArray();
        openingBracketsPositions = new ArrayList<>();
        closingBracketsPositions = new ArrayList<>();
        for(int i = 0; i < characterSequence.length; i++){
            if (characterSequence[i] == '{') openingBracketsPositions.add(i);
            else if (characterSequence[i] == '}') closingBracketsPositions.add(i);
        }
    }
}
